.TH CDDBGET 1 "March 1, 2005"
.\"
.SH NAME
cddbget \- gets CDDB info of a CD
.\"
.SH SYNOPSIS
.B cddbget
.RI [ options ]
.\"
.SH DESCRIPTION
The \fBcddbget\fP script can be used to get CDDB info of a CD. It can handle
several output formats.
.\"
.SH OPTIONS
.TP
.B no argument
gets CDDB info of CD in your drive
.TP
.B \-c
device (other than default device)
.TP
.B \-o
offline mode - just stores CD info
.TP
.B \-d
output in xmcd format
.TP
.B \-s
save in xmcd format
.TP
.B \-i
db. one of: mysql, pg, oracle, sqlite
.TP
.B \-O
overwrite file or db
.TP
.B \-t
output toc
.TP
.B \-l
output lame command
.TP
.B \-f
http mode (e.g. through firewalls)
.TP
.B \-F
some stateful firewalls/http proxies need additional newlines
.TP
.B \-g
get CDDB info for stored CDs
.TP
.B \-I
non interactive mode
.TP
.B \-H
CDDB hostname
.TP
.B \-C
use local cache
.TP
.B \-R
readonly cache
.TP
.B \-G
cache has not the diskid as filenames (much slower)
.TP
.B \-P
cache path (default: /tmp/xmcd)
.TP
.B \-D
put CDDB_get in debug mode
.\"
.SH SEE ALSO
.BR CDDB_get (3pm).
.\"
.SH AUTHOR
CDDB_get was written by Armin Obersteiner <armin@xos.net>.
.PP
This manual page was written by Lucas Wall <lwall@debian.org>, for the Debian
project (but may be used by others).
